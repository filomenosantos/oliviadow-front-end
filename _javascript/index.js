// JavaScript Document
$("document").ready(function(){
	
	$('#acord p').on('click', function(){
			$(this).parent().siblings().children().next().slideUp(1000);
			$(this).next().slideDown(1000);	
		}
	);
	$('.abNews').on('click', function(){
			$(this).parent().siblings().children().next().slideUp(2000);
			$(this).next().delay(2100).slideDown(4000);
	});

	setInterval("slidePix()", 5000);
	
});

function slidePix(){
	var curPic = $('#intro div.active');
	var nextPic = curPic.next();
	if (nextPic.length == 0){
		nextPic = $('#intro div:first');
	}
	
	curPic.removeClass('active').addClass('prev');
	nextPic.css({opacity:0.0}).addClass('active').animate({opacity:1.0},2000,function(){
		curPic.removeClass('prev');
	});
}

function tableHide(){
	$('table').hide();
}