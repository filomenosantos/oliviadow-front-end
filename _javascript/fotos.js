// JavaScript Document

function closeAlbum(){
	$('.background, .box').animate({'opacity':'.0'}, 500, 'linear', function(){
		$('.background, .box').css('display','none');
	});
	$('.slides ul').cycle('stop');
}

function showAlbum1(){
	$('#background1, #box1').animate({'opacity':'1.00'},500,'linear');
	$('#background1, #box1').css('display', 'block');
	$('.slides ul').cycle({
		fx:'fade',
		speed: 2000,
		timeout: 2000,
		prev:'.left',
		next:'.right',
	});
}

function showAlbum2(){
	$('#background2, #box2').animate({'opacity':'1.00'},500,'linear');
	$('#background2, #box2').css('display', 'block');
	$('.slides ul').cycle({
		fx:'fade',
		speed: 2000,
		timeout: 2000,
		prev:'.left',
		next:'.right',
	});
}

function showAlbum3(){
	$('#background3, #box3').animate({'opacity':'1.00'},500,'linear');
	$('#background3, #box3').css('display', 'block');
	$('.slides ul').cycle({
		fx:'fade',
		speed: 2000,
		timeout: 2000,
		prev:'.left',
		next:'.right',
	});
}

function showAlbum4(){
	$('#background4, #box4').animate({'opacity':'1.00'},500,'linear');
	$('#background4, #box4').css('display', 'block');
	$('.slides ul').cycle({
		fx:'fade',
		speed: 2000,
		timeout: 2000,
		prev:'.left',
		next:'.right',
	});
}

function showAlbum5(){
	$('#background5, #box5').animate({'opacity':'1.00'},500,'linear');
	$('#background5, #box5').css('display', 'block');
	$('.slides ul').cycle({
		fx:'fade',
		speed: 2000,
		timeout: 2000,
		prev:'.left',
		next:'.right',
	});
}

function showAlbum6(){
	$('#background6, #box6').animate({'opacity':'1.00'},500,'linear');
	$('#background6, #box6').css('display', 'block');
	$('.slides ul').cycle({
		fx:'fade',
		speed: 2000,
		timeout: 2000,
		prev:'.left',
		next:'.right',
	});
}

function showAlbum7(){
	$('#background7, #box7').animate({'opacity':'1.00'},500,'linear');
	$('#background7, #box7').css('display', 'block');
	$('.slides ul').cycle({
		fx:'fade',
		speed: 2000,
		timeout: 2000,
		prev:'.left',
		next:'.right',
	});
}

function showAlbum8(){
	$('#background8, #box8').animate({'opacity':'1.00'},500,'linear');
	$('#background8, #box8').css('display', 'block');
	$('.slides ul').cycle({
		fx:'fade',
		speed: 2000,
		timeout: 2000,
		prev:'.left',
		next:'.right',
	});
}

function showAlbum9(){
	$('#background9, #box9').animate({'opacity':'1.00'},500,'linear');
	$('#background9, #box9').css('display', 'block');
	$('.slides ul').cycle({
		fx:'fade',
		speed: 2000,
		timeout: 2000,
		prev:'.left',
		next:'.right',
	});
}

